package net.jahez.jahezchallenge.utilities

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import net.jahez.jahezchallenge.screens.homePage.models.Restaurant
import net.jahez.jahezchallenge.screens.homePage.room.RestaurantsDao


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

@Database(entities = [Restaurant::class], version = 1, exportSchema = false)
abstract class RoomHelper : RoomDatabase() {
    abstract fun restaurantsDao(): RestaurantsDao

    companion object {
        @Volatile
        private var INSTANCE: RoomHelper? = null

        fun getDatabase(context: Context): RoomHelper {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RoomHelper::class.java,
                    "restaurants"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}