package net.jahez.jahezchallenge.utilities.network

import net.jahez.jahezchallenge.screens.homePage.models.Restaurant
import retrofit2.http.GET
import retrofit2.Call;
import retrofit2.http.Body

import retrofit2.http.POST


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

interface EndPoints {
    @GET("restaurants.json")
    fun getRestaurants(): Call<ArrayList<Restaurant>>
}