package net.jahez.jahezchallenge.screens.settingsPage.ui

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import net.jahez.jahezchallenge.MainActivity
import net.jahez.jahezchallenge.R
import net.jahez.jahezchallenge.ui.theme.Red200
import net.jahez.jahezchallenge.ui.theme.listItemBG
import net.jahez.jahezchallenge.utilities.VariablesUtils
import net.jahez.jahezchallenge.utilities.setLocaleLang


@Composable
fun SettingsPage(context: Context) {
    Card(modifier = Modifier.fillMaxWidth().padding(top = 12.dp), backgroundColor = listItemBG) {
        Row(horizontalArrangement = Arrangement.SpaceBetween, modifier = Modifier.padding(all = 16.dp)) {
            Text(stringResource(R.string.language))
            segmentControls(context)
        }
    }
}

@Composable
fun segmentControls(context: Context) {
    val index = remember { mutableStateOf(if (VariablesUtils.language == "ar") 0 else 1) }

    Row(modifier = Modifier.padding(horizontal = 8.dp).clip(CircleShape)) {
        Box(
            modifier = Modifier.width(80.dp).height(30.dp).background(
                color = if (index.value == 0) Red200 else Color.White
            ).clickable {
                index.value = 0
                setLocaleLang("ar", context)
                intent(context)
            }, contentAlignment = Alignment.Center
        ) {
            Text(stringResource(R.string.arabic), color = if (index.value == 0) Color.White else Color.Black)
        }
        Box(
            modifier = Modifier.width(80.dp).height(30.dp).background(
                color = if (index.value == 1) Red200 else Color.White
            ).clickable {
                index.value = 1
                setLocaleLang("en", context)
                intent(context)
            }, contentAlignment = Alignment.Center
        ) {
            Text(stringResource(R.string.english), color = if (index.value == 1) Color.White else Color.Black)
        }
    }
}

private fun intent(context: Context) {
    Handler(Looper.getMainLooper()).postDelayed({
        context.startActivity(Intent(context, MainActivity::class.java))
    }, 2000)
}



