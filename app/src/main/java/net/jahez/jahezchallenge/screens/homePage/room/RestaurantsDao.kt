package net.jahez.jahezchallenge.screens.homePage.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow
import net.jahez.jahezchallenge.screens.homePage.models.Restaurant


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

@Dao
interface RestaurantsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRestaurant(vararg restaurant: Restaurant)

    @Query("select * from restaurants")
    fun getRestaurants(): Flow<List<Restaurant>>

    @Query("DELETE FROM restaurants")
    suspend fun deleteAll()
}