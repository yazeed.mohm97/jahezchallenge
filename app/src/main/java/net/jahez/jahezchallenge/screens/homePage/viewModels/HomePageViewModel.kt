package net.jahez.jahezchallenge.screens.homePage.viewModels

import android.util.Log
import androidx.activity.ComponentActivity
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.jahez.jahezchallenge.drawer.DrawerViewModel
import net.jahez.jahezchallenge.screens.homePage.models.Restaurant
import net.jahez.jahezchallenge.screens.homePage.network.HomePageRepository
import net.jahez.jahezchallenge.screens.homePage.room.RestaurantsRepository
import net.jahez.jahezchallenge.utilities.RoomHelper
import kotlin.coroutines.CoroutineContext

/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class HomePageViewModel : ViewModel() {
    var restaurants = mutableStateListOf<Restaurant>()
        private set


    val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    fun initRestaurants(context: ComponentActivity, dViewModel: DrawerViewModel) {
        Log.i("initRestaurants", "im in initRestaurants")
        if (restaurants.size != 0) {
            Log.i("initRestaurants", "im in if statement")
            return
        }
        val repository = HomePageRepository().getInstance()
        repository!!.getRestaurants().observe(context, { data ->
            dViewModel.loading.value = false
            restaurants.addAll(data!!.sortedByDescending { it.offer })
            insertToRoom(context, data)
        })

    }

    private fun insertToRoom(context: ComponentActivity, rests: ArrayList<Restaurant>) {
        val database by lazy { RoomHelper.getDatabase(context) }
        val dRepository by lazy { RestaurantsRepository(database.restaurantsDao()) }

        CoroutineScope(coroutineContext).launch {
            rests.forEach { item ->
                dRepository.insert(item)
            }
        }
    }

    fun getFromRoom(context: ComponentActivity, dViewModel: DrawerViewModel) {
        val database by lazy { RoomHelper.getDatabase(context) }
        val dRepository by lazy { RestaurantsRepository(database.restaurantsDao()) }
        val allRestaurants: LiveData<List<Restaurant>> = dRepository.allRestaurants.asLiveData()
        allRestaurants.observe(context, { data ->
            dViewModel.loading.value = false
            data!!.sortedByDescending { it.offer }
            restaurants.addAll(data.sortedByDescending { it.offer })
        })
    }
}

