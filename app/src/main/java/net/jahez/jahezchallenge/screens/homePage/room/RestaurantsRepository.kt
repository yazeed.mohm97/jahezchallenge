package net.jahez.jahezchallenge.screens.homePage.room

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow
import net.jahez.jahezchallenge.screens.homePage.models.Restaurant


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class RestaurantsRepository(private val restaurantsDao: RestaurantsDao) {
    val allRestaurants: Flow<List<Restaurant>> = restaurantsDao.getRestaurants()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(restaurant: Restaurant) {
        restaurantsDao.insertRestaurant(restaurant)
    }
}