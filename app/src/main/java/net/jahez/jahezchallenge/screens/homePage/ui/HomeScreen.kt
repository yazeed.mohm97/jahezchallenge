package net.jahez.jahezchallenge.screens.homePage.ui

import androidx.compose.foundation.Image
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import net.jahez.jahezchallenge.screens.homePage.models.Restaurant
import net.jahez.jahezchallenge.ui.theme.listItemBG
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarStyle
import net.jahez.jahezchallenge.R
import net.jahez.jahezchallenge.ui.theme.green


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

@Composable
fun HomeScreen(items: List<Restaurant>) {
    Scaffold {
        restsListView(items)
    }
}

@Composable
fun restsListView(items: List<Restaurant>) {
    LazyColumn {
        items(items) { restaurant ->
            restItem(restaurant)
        }
    }
}

/**
 * restaurant item: show item in lazyColumn in cards, sorted by offers
 */
@Composable
fun restItem(restaurant: Restaurant) {
    Card(
        backgroundColor = listItemBG,
        modifier = Modifier.padding(vertical = 4.dp, horizontal = 8.dp)
    ) {
        Box {
            Row(
                modifier = Modifier.padding(12.dp).animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
            ) {
                Surface(
                    modifier = Modifier.size(128.dp),
                    color = MaterialTheme.colors.onSurface.copy(alpha = 0.2f),
                    shape = RoundedCornerShape(12.dp)
                ) {
                    Image(
                        painter = rememberImagePainter(data = restaurant.image, builder = {
                            crossfade(true)
                        }),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,
                        modifier = Modifier.size(128.dp)
                    )
                }
                Column(modifier = Modifier.weight(1f).padding(12.dp)) {
                    Text(restaurant.name!!, fontSize = 18.sp, style = TextStyle(fontWeight = FontWeight.Bold))
                    Text(restaurant.hours!!, fontSize = 14.sp)
                    RatingBar(
                        value = restaurant.rating.toFloat(),
                        ratingBarStyle = RatingBarStyle.HighLighted,
                        size = 15.dp,
                        padding = 0.dp,
                        onValueChange = {}
                    ) {
                    }
                }

            }

            if (restaurant.hasOffer)
                Surface(modifier = Modifier.align(Alignment.TopEnd), color = green) {
                    Text("${stringResource(R.string.discount)} ${restaurant.offer!!}", fontSize = 12.sp, style = TextStyle(color = Color.White), modifier = Modifier.padding(4.dp))
                }
        }
    }
}
