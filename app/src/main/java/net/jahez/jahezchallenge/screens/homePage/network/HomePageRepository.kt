package net.jahez.jahezchallenge.screens.homePage.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import net.jahez.jahezchallenge.screens.homePage.models.Restaurant
import net.jahez.jahezchallenge.utilities.network.EndPoints
import net.jahez.jahezchallenge.utilities.network.NetworkClient
import org.jetbrains.annotations.NotNull
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

import retrofit2.Retrofit


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class HomePageRepository {
    private var instance: HomePageRepository? = null

    fun getInstance(): HomePageRepository? {
        if (instance == null) instance = HomePageRepository()
        return instance
    }

    fun getRestaurants(): MutableLiveData<ArrayList<Restaurant>?> {
        Log.i("getRestaurants", "im in getRestaurants")
        val restaurants: MutableLiveData<ArrayList<Restaurant>?> = MutableLiveData<ArrayList<Restaurant>?>()
        val retrofit: Retrofit? = NetworkClient.getRetrofitClient()
        val api: EndPoints = retrofit!!.create(EndPoints::class.java)
        val getData: Call<ArrayList<Restaurant>> = api.getRestaurants()
        getData.enqueue(object : Callback<ArrayList<Restaurant>?> {
            override fun onResponse(call: Call<ArrayList<Restaurant>?>, response: Response<ArrayList<Restaurant>?>) {
                if (response.isSuccessful) {
                    Log.i("getRestaurants", response.body().toString())
                    restaurants.value = response.body()
                } else {
                    Log.i("getRestaurants", response.message().toString())
                    restaurants.value = null
                }
            }

            override fun onFailure(call: Call<ArrayList<Restaurant>?>, t: Throwable) {
                Log.i("getRestaurants", t.message!!)
                restaurants.value = null
            }

        })
        return restaurants
    }

}