package net.jahez.jahezchallenge.ui.theme

import androidx.compose.ui.graphics.Color

val Red200 = Color(0xFFb30000)
val Red500 = Color(0xFFa10000)
val Red700 = Color(0xFF800000)
val listItemBG = Color(0xFFebebeb)
val green = Color(0xFF2C6000)
val Teal200 = Color(0xFF03DAC5)