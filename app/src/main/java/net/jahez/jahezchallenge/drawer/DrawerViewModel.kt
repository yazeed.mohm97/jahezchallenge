package net.jahez.jahezchallenge.drawer

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

class DrawerViewModel : ViewModel() {
    var title = mutableStateOf("")
    var loading = mutableStateOf(true)
}