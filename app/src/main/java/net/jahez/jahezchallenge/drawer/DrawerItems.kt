package net.jahez.jahezchallenge.drawer

import android.content.Context
import android.provider.Settings.System.getString
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Home
import androidx.compose.material.icons.rounded.Settings
import androidx.compose.ui.graphics.vector.ImageVector
import net.jahez.jahezchallenge.MainActivity
import net.jahez.jahezchallenge.R


/**
 * created by: yazeed nasrullah
 * email: yazeed.mohm97@gmail.com
 */

sealed class DrawerScreens(val title: Int, val icon: ImageVector, val tag: String) {
    object Home : DrawerScreens(R.string.home, Icons.Rounded.Home, "Home")
    object Settings : DrawerScreens(R.string.settings, Icons.Rounded.Settings, "Settings")
}

