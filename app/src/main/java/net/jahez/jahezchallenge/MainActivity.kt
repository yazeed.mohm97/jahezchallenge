package net.jahez.jahezchallenge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.rounded.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.launch
import net.jahez.jahezchallenge.drawer.DrawerScreens
import net.jahez.jahezchallenge.drawer.DrawerViewModel
import net.jahez.jahezchallenge.ui.theme.JahezTheme
import androidx.navigation.compose.composable
import net.jahez.jahezchallenge.screens.homePage.ui.HomeScreen
import net.jahez.jahezchallenge.screens.homePage.viewModels.HomePageViewModel
import net.jahez.jahezchallenge.screens.settingsPage.ui.SettingsPage
import net.jahez.jahezchallenge.utilities.internetCheck

class MainActivity : ComponentActivity() {
    private val dViewModel by viewModels<DrawerViewModel>()
    private val homeViewModel by viewModels<HomePageViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JahezTheme {
                dViewModel.title.value = getString(R.string.home)
                app()
            }
        }

    }


    @Composable
    fun app() {
        drawer()
    }

    @Composable
    private fun drawer() {
        val navController = rememberNavController()
        val scaffoldState = rememberScaffoldState()
        val scope = rememberCoroutineScope()

        Scaffold(
            scaffoldState = scaffoldState,
            topBar = {
                TopAppBar {
                    IconButton(onClick = {
                        scope.launch {
                            scaffoldState.drawerState.apply {
                                if (isOpen) close() else open()
                            }
                        }
                    }) {
                        Icon(Icons.Filled.Menu, "")
                    }

                    Row(modifier = Modifier.padding(start = 12.dp)) {
                        Text(dViewModel.title.value, fontSize = 18.sp, fontWeight = FontWeight.Bold)
                    }
                }
            },
            drawerContent = {
                Surface(color = Color.Gray, modifier = Modifier.padding(top = 25.dp, start = 16.dp).clip(CircleShape).size(100.dp)) {
                    Icon(Icons.Rounded.Person, contentDescription = "person", tint = Color.White, modifier = Modifier.padding(8.dp))
                }
                Text("Yazeed Mohammad", modifier = Modifier.padding(16.dp))
                Divider()

                Column(modifier = Modifier.padding(vertical = 16.dp, horizontal = 16.dp)) {
                    screens.forEach { screen ->
                        Row(verticalAlignment = Alignment.CenterVertically, modifier = Modifier.clickable {
                            scope.launch { scaffoldState.drawerState.close() }
                            dViewModel.title.value = getString(screen.title)
                            navController.navigate(screen.tag)
                        }) {
                            Icon(screen.icon, tint = Color.Gray, contentDescription = getString(screen.title), modifier = Modifier.padding(8.dp))
                            Text(getString(screen.title), modifier = Modifier.weight(1f))
                        }
                    }
                }
            },

            content = { innerPadding ->
                NavHost(
                    navController = navController,
                    startDestination = DrawerScreens.Home.tag,
                    modifier = Modifier.padding(innerPadding)
                ) {
                    composable(DrawerScreens.Home.tag) {
                        homePageScreen(homeViewModel)
                    }
                    composable(DrawerScreens.Settings.tag) {
                        settingsPageScreen()
                    }
                }
            },
        )
    }

    private val screens = listOf(
        DrawerScreens.Home,
        DrawerScreens.Settings,
    )


    /**
     * get data from http if internet connection available, or from room if not available
     */
    @Composable
    fun homePageScreen(homeViewModel: HomePageViewModel) {
        if (dViewModel.loading.value) {
            LaunchedEffect(Unit) {
                if (internetCheck(this@MainActivity))
                    homeViewModel.initRestaurants(this@MainActivity, dViewModel)
                else
                    homeViewModel.getFromRoom(this@MainActivity, dViewModel)

            }
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                CircularProgressIndicator()
            }
        } else {
            HomeScreen(homeViewModel.restaurants)
        }
    }

    @Composable
    fun settingsPageScreen() {
        SettingsPage(this)
    }


}